class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

    # def __repr__(self):
    #     return self.value

    def __str__(self):
        return f'{self.value}'

"""
    Root - 7

        7
       / \
     23    8
          / \
         21 15
"""


def breadth_search(root, value):
    queue = [{"node": root, "level": 0}]  # FIFO

    while len(queue):
        curr = queue.pop(0)  # {"node": 21, "level": 0}

        print("level: ", curr.get("level"), "value", curr.get("node").value)

        if curr.get("node").value == value:  # 21 == 21
            return True

        if curr.get("node").left:
            queue.append({"node": curr.get("node").left, "level": curr.get("level") + 1})  # [15]

        if curr.get("node").right:
            queue.append({"node": curr.get("node").right, "level": curr.get("level") + 1})  # [15]

    return False


root = Node(7)
root.left = Node(23)
root.right = Node(8)
root.left.left = Node(5)
root.left.right = Node(4)
root.right.left = Node(21)
root.right.right = Node(15)

result = breadth_search(root, 21)
print(result)
