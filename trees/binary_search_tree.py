class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

    # def __repr__(self):
    #     return self.value

    def __str__(self):
        return f'{self.value}'


"""
    Root - 7

        7
       / \
     23    8
    / \   / \
   5  4  21 15
   
        7    
       / \
     6     23
    / \   / \
   4  5  15 21
"""

root = Node(7)
root.left = Node(6)
root.right = Node(23)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(15)
root.right.right = Node(21)


def depth_search(root, value):
    if root is None:
        return False

    print(root.value)

    if root.value == value:
        return True

    if root.value < value:
        return depth_search(root.right, value)

    return depth_search(root.left, value)

result = depth_search(root, 5)
print(result)
