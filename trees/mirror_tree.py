class TreeNode:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __str__(self):
        return f'{self.data}'


root = TreeNode(1)
root.left = TreeNode(2)
root.right = TreeNode(3)
root.left.left = TreeNode(4)
root.left.right = TreeNode(5)
root.right.left = TreeNode(6)
root.right.right = TreeNode(7)

# root zustane stejny
# prohozeni


def mirror_binary_tree(root): # 1
    if root is None:
        return
    # root

    root.left, root.right = root.right, root.left  # left, right = 3, 2
    mirror_binary_tree(root.left)
    mirror_binary_tree(root.right)


def pre_order(root):
    if root:
        print(root.data)
        pre_order(root.left)
        pre_order(root.right)


mirror_binary_tree(root)
pre_order(root)

# Puvodni strom
#     1
#    / \
#   2   3
#  / \ / \
# 4  5 6  7


# Otoceny strom
#     1
#    / \
#   3   2
#  / \ / \
# 7  6 5  4

# 1, 3, 7, 6, 2, 5, 4
