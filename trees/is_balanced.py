class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __str__(self):
        return f'{self.data}'


def binary_tree_depth(root):
    if root is None:  # Base
        return 0

    left_depth = binary_tree_depth(root.left)
    right_depth = binary_tree_depth(root.right)

    return max(left_depth, right_depth) + 1


def is_balanced(node):
    if node is None:
        return True

    left_height = binary_tree_depth(node.left)
    right_height = binary_tree_depth(node.right)
    # print(left_height, right_height)

    print(left_height, right_height)
    if abs(left_height - right_height) > 1:
        return False

    print(node.left, node.right, "xxx")

    # return is_balanced(node.left) and is_balanced(node.right)

"""
         1
        /  \
       2     3 
      / 
     4 
    / 
   6
"""

root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.right.right = Node(8)
root.right.right.right = Node(20)
root.right.right.right.left = Node(14)
root.left.left = Node(4)
root.left.left.left = Node(6)

# check if the tree is balanced
tree_is_balanced = is_balanced(root)
print(tree_is_balanced)
