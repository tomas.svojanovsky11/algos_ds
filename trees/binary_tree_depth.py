class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __str__(self):
        return f'{self.data}'


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)

""""
    1
   / \
  2     3
 /
4
Nejvetsi pocet hran + 1
"""


def binary_tree_depth(root):
    if root is None:  # Base
        return 0

    left_depth = binary_tree_depth(root.left)
    right_depth = binary_tree_depth(root.right)

    return max(left_depth, right_depth) + 1


result = binary_tree_depth(root)
print(result)