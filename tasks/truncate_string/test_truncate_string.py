from tasks.truncate_string.truncate_string import truncate_string


def test_truncate_string():
    result = truncate_string("Peter Piper picked a peck of pickled peppers", 11)
    assert result == "Peter Piper..."
