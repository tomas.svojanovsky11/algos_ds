from tasks.level_1.reverse_string.reverse_string import reverse_string


def test_reverse_string():
    result = reverse_string("hello")
    assert result == "olleh"

    result = reverse_string("brown rabbit")
    assert result == "tibbar nworb"
