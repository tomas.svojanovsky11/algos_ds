from tasks.title_case.title_case import title_case


def test_title_case():
    result = title_case("I'm a little tea pot")
    assert result == "I'm A Little Tea Pot"
    result = title_case("HERE IS MY HANDLE HERE IS MY SPOUT")
    assert result == "Here Is My Handle Here Is My Spout"
