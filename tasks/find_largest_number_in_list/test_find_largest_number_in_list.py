from tasks.find_largest_number_in_list.find_largest_number_in_list import find_number


def test_find_number():
    result = find_number([2, 5, 1, 9, 3, 1, 5, 4])
    assert result == 9
