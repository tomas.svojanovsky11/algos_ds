from tasks.find_longest_word_in_string.find_longest_word_in_string import find_word


def test_find_word():
    text = "The quick brown fox jumped over the lazy dog"
    result = find_word(text)
    assert result == "jumped"
