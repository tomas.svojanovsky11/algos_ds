def sum_char_codes(n: str) -> int:
    sum = 0
    for letter in n:
        sum += ord(letter)

    for letter in n:
        sum += ord(letter)

    return sum
