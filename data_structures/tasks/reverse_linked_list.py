# class Node:
#     def __init__(self, value=None):
#         self.next = None
#         self.value = value
from data_structures.linked_list import LinkedList


# Preferovana verze - iterativni

def reverse_link_list_iterative(head):
    prev, curr = None, head

    while curr:
        nxt = curr.next  # Node B
        curr.next = prev  # None
        # (curr, prev) = (nxt, curr)  # Node A
        prev = curr
        curr = nxt  # Node B

    return prev

    # prev, curr = None, head

    # if head is None:
    #     return None
    #
    # if head.next:
    #     reverse_link_list(head.next)


def reverse_linked_list_recursive(head):
    if head is None:
        return None

        # E.next = None

    new_head = head  # E
    if head.next:
        new_head = reverse_linked_list_recursive(head.next)
        head.next.next = head
    head.next = None

    return new_head


linked_list = LinkedList()
# linked_list.print()
linked_list.insert_at_end("A")
linked_list.insert_at_end("B")
linked_list.insert_at_end("C")
linked_list.insert_at_end("D")
linked_list.insert_at_end("E")


result = reverse_linked_list_recursive(linked_list.head)
# result = reverse_link_list_iterative(linked_list.head)
linked_list.head = result
