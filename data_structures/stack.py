class Node:
    def __init__(self, value=None):
        self.value = value
        self.prev = None


class Stack:
    def __init__(self):
        self.head = None
        self.length = 0

    def pop(self):
        self.length = max(0, self.length - 1)  # 0,1 -> 0 | 0,7 -> 7

        if self.length == 0:
            head = self.head  # bud to bude None a nebo nejaky element
            self.head = None
            return head if head.value else None

        head = self.head
        # self.head = None

        self.head = head.prev

        return head.value

    def push(self, value):
        new_node = Node(value)
        self.length += 1

        if self.head is None:
            self.head = new_node
            return

        # aktualni hlavicka pujde dolu -> prev novy element
        # novy element pujde do hlavicky
        new_node.prev = self.head  # Aktualni hlavicka je 4. Ta pujdu o krok dozadu
        # proto bude prev u noveho elementu
        self.head = new_node # nova hlavicka se stava nove pridany element

    def peek(self):
        return self.head.value if self.head else None

    def print(self):
        node = self.head

        while node is not None:
            print(node.prev)
            node = node.prev
