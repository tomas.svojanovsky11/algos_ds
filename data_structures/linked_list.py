class Node:
    def __init__(self, data):
        self.data = data  # A
        self.next = None

    def __repr__(self):
        return f'{self.data}'


class LinkedList:
    def __init__(self):
        self.head = None

    def insert_at_beginning(self, new_data):
        new_node = Node(new_data)

        new_node.next = self.head # novy element ukazuje na puvodni hlavicku
        self.head = new_node  # head ukazuje na novy element

    def print(self):
        node = self.head
        while node is not None:
            print(node.data)
            node = node.next

    def insert_at_end(self, new_data):
        new_node = Node(new_data)

        # linked list je prazdny
        if self.head is None:
            self.head = new_node
            return

        # doskakej az na konec a vloz mi tam prvek
        last = self.head
        while last.next:
            last = last.next

        last.next = new_node

    def insert_after(self, prev_node, data):
        new_node = Node(data)
        new_node.next = prev_node.next # B posilate do F
        prev_node.next = new_node # do A posilate F

    def delete_node(self, position):
        if self.head is None:
            return

        temp = self.head

        if position == 0:
            self.head = temp.next
            temp = None
            return

        for i in range(position - 1):
            temp = temp.next
            if temp is None:
                break

        if temp is None:
            return

        if temp.next is None:
            return

        # Vymena - opacna akce k insert after
        next = temp.next.next
        temp.next = None
        temp.next = next


# def reverse_link_list(head):
#     if head is None:
#         return None
#
#     if head.next:
#         reverse_link_list(head.next)

def reverse_linked_list(head):
    # prev, curr = None, head
    #
    # while curr:
    #     nxt = curr.next  # B, C
    #     curr.next = prev  # None, A
    #     prev = curr  # A
    #     curr = nxt  # B
    #
    # return prev  # E

    # A -> B -> C -> D -> E -> None
    # A -> None B -> C -> D -> E -> None
    # nxt = B
    # B -> A -> None C -> D -> E -> None

    if head is None:
        return None

    # E.next = None

    new_head = head  # E
    if head.next:
        new_head = reverse_linked_list(head.next)
        head.next.next = head
    head.next = None
    # D.next.next = D
    # C.next.next = C

    print(new_head, "z")
    return new_head

    # head.next.next = B head = D
    # head.next.next = B head = C
    # head.next.next = B head = B
    # head.next.next = B head = A

    # A -> B -> C -> D -> E -> None
    # E -> None
    # A -> B -> C  E -> D -> C
    # A -> B -> C -> D  E -> D
    # C.next = D C.next.next = E

linked_list = LinkedList()
# linked_list.print()
linked_list.insert_at_end("A")
linked_list.insert_at_end("B")
linked_list.insert_at_end("C")
linked_list.insert_at_end("D")
linked_list.insert_at_end("E")
# linked_list.insert_at_end(5)
# linked_list.delete_node(2)
# linked_list.print()  # HEAD -> 3 -> 2 -> 1 -> 5
# linked_list.insert_after(linked_list.head.next, 99)
# linked_list.print()  # HEAD -> 3 -> 2 -> 99 -> 5
result = reverse_linked_list(linked_list.head)
# linked_list.head = result
# linked_list.print()
# print(result.next)

# result = reverse_link_list(linked_list.head)
# print(result)
# linked_list.print()

