# Do priste opravit/ujasnit
class Node:
    def __init__(self, value=None):
        self.value = value
        self.next = None


class Queue:
    def __init__(self):
        self.length = 0
        self.head = None
        self.tail = None

    def enqueue(self, value):
        self.length += 1
        new_node = Node(value)

        if self.tail is None:
            self.tail = self.head = new_node
            return

        self.tail.next = new_node
        self.tail = new_node
        # new_node.next = self.tail  # mrkni na to

    def dequeue(self):
        if self.head is None:
            return

        self.length -= 1
        head = self.head
        self.head = self.head.next
        return head.value

    def peek(self):
        return self.head.value if self.head else None


queue = Queue()
queue.enqueue(1)
queue.enqueue(2)
result = queue.peek()
print(result)
