def chunky_monkey(array, size):
    result = []

    for i in range(0, len(array), size):  # 0 2 / 2 4 / 4 6
        result.append(array[i:i + size])

    return result

    # return chunky_monkey(array, size, i + size)


result = chunky_monkey([0, 1, 2, 3, 4, 5], 2)
print(result)  # [[0, 1], [2, 3], [4, 5]]
result = chunky_monkey([0, 1, 2, 3, 4, 5, 6], 3)
print(result)  # [[0, 1, 2], [3, 4, 5], [6]]
