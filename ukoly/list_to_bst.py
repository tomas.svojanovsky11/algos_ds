array = [1, 2, 3, 4, 5, 6, 7]
"""
Constructed balanced BST is
       4
      / \
     2    6
    / \  / \
   1  3  5  7
"""


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __str__(self):
        return f'{self.data}'


def sorted_list_to_bst(array):
    if not array:
        return None

    mid = len(array) // 2
    root = Node(array[mid])  # 4

    root.left = sorted_list_to_bst(array[:mid]) # [1, 2, 3]
    root.right = sorted_list_to_bst(array[mid + 1:])  # [5, 6, 7]

    return root


def pre_order(node):
    if not node:
        return

    print(node.data)
    pre_order(node.left)
    pre_order(node.right)


result = sorted_list_to_bst(array)
pre_order(result)

