from typing import List


# vezmu prvni prvek
# budu prochazet list o index vetsi (krome prvniho prvku)
# kdyz najdu cislo, ktere je vetsi, tak ulozim do promenne a opakuji algoritmus

def find_largest_number(numbers: List[int]) -> int:
    largest_num = numbers[0]  # 9
    for item in numbers[1:]:  # [5, 1, 9, 3, 1, 5, 4]
        if item > largest_num:  # 9 > 5
            largest_num = item

    return largest_num


print(find_largest_number([2, 5, 1, 9, 3, 1, 5, 4]))
