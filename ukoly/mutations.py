# Vratte True pokud vsechny znaky v druhem stringu jsou obsazeny v prvnim stringu

def mutation(array):
    seed = set(array[0].lower())
    test_case = set(array[1].lower())
    # seed = array[0].lower()
    # test_case = array[1].lower()

    return len(seed.intersection(test_case)) == len(test_case)

    # for letter in test_case:
    #     try:
    #         seed.index(letter)
    #     except ValueError:
    #         return False
    #
    # return True


result = mutation(["hello", "hey"])
print(result)  # False
result = mutation(["hello", "Hello"])
print(result)  # True
result = mutation(["zyxwvutsrqponmlkjihgfedcba", "qrstu"])
print(result)  # True
result = mutation(["Mary", "Army"])
print(result)  # True
result = mutation(["floor", "for"])
print(result)  # True
result = mutation(["hello", "neo"])
print(result)  # False
