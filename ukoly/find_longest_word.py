def find_longest_word(text: str) -> str:
    longest_word = ""  # The
    for word in text.split(" "):  # ["The", "quick", "brown"...]
        if len(word) > len(longest_word):  # len(The) > len("") 3 > 0
            longest_word = word

    return longest_word


print(find_longest_word("The quick brown fox jumped over the lazy dog"))
