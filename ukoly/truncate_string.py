# Dobry den jak se mate?
# Dobry den jak...

# je num mensi nez delka textu
# pokud ano tak osekej retezec a vypis ...
# jinak vrat puvodni retezec

def truncate_string(text: str, num: int) -> str:
    if len(text) > num:
        return text[:num] + "..."
    else:
        return text


print(truncate_string("Dobry den jak se mate?", 10))
