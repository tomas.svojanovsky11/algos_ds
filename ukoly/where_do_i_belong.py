# Vratte nejmensi index na ktery lze vlozit prvek do listu, jinak pujde na konec
# Cislo musi byt vetsi nez predchozi cislo a mensi nez nasledujici

def where_do_i_belong(array, num):
    for i in range(len(array)):
        if i == 0 and array[i] > num:
            return i
        elif (not len(array) - 1 == i) and array[i - 1] <= num <= array[i]:  # 30 < 35 < 40
            return i

    return len(array)


result = where_do_i_belong([10, 20, 30, 40, 50], 11)
print(result)  # 3
result = where_do_i_belong([10, 20, 30, 40, 50], 30)
print(result)  # 2
result = where_do_i_belong([], 1)
print(result)  # 0
result = where_do_i_belong([2, 5, 10], 15)
print(result)  # 3
result = where_do_i_belong([5, 3, 20, 3], 5)
print(result)  # 0
