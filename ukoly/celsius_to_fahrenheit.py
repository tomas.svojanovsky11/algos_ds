# Celsius -> Fahrenheit 9/5 + 32
def convert_to_fahrenheit(celsius):
    return celsius * 9/5 + 32


# class Human:
#     def __init__(self):
#         self.height = 0
#
#     def getHeight(self):
#         return self.he

#
# def to_int(val):
#     return int(val)

# Pure function

# list = []
#
# def fn(list):
#     list.append(1)
#
# fn(list)


print(convert_to_fahrenheit(-30))  # -22
print(convert_to_fahrenheit(-10))  # 14
print(convert_to_fahrenheit(0))  # 32
print(convert_to_fahrenheit(20))  # 68
print(convert_to_fahrenheit(30))  # 86
