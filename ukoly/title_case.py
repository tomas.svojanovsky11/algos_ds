import re


def title_case(text: str) -> str:
    result = []

    # "xUhhg"
    # "xuHHg".lower() == "xuhhg"
    # "xuhhg"

    # Dobry den
    # words = text.lower().split(" ")  # ["Dobry", "den"]
    # for i in range(0, len(words)):
    #     word = words[i]
    #     result.append(word.replace(word[0], word[0].capitalize(), 1))
    #
    # return " ".join(result)
    return re.sub(
        r"[A-Za-z]+('[A-Za-z]+)?",
        lambda word: word.group(0).capitalize(),
        text
    )




