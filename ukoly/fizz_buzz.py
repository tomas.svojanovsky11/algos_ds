# Vytvor list ve kterem budou cisla od 1 do 100
# Kdyz je cislo delitelne 3, tak misto cisla bude Fizz
# Kdyz je cislo delitelne 5, tak misto cisla bude Buzz
# Kdyz je cislo delitelne 3 a 5, tak misto cisla bude FizzBuzz

# [1, 2, 3, 4, 5, 6, 7, 8. 9, 10]
# [1, 2, "Fizz", 4, "Buzz", "Fizz", 7, 8, "Fizz", "Buzz", ..... FizzBuzz]

def fizz_buzz():
    result = []

    for n in range(1, 101):
        if n % 3 == 0 and n % 5 == 0:
            result.append("FizzBuzz")
        elif n % 3 == 0:
            result.append("Fizz")
        elif n % 5 == 0:
            result.append("Buzz")
        else:
            result.append(n)

    return result


def fizz_buzz_2():
    result = []

    for n in range(1, 101):
        if n % 3 == 0:
            result.append("Fizz")
        elif n % 5 == 0:
            result.append("Buzz")
        elif n % 3 == 0 and n % 5 == 0:
            result.append("FizzBuzz")
        else:
            result.append(n)

    return result


print(fizz_buzz())
print(fizz_buzz_2())

