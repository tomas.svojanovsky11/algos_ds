chessboard_size = 8

"""
  0 1 2 3 4 5 6 7
0 f x x x x x x x 
1 x x x x x x x x 
2 x f x x x x x x 
3 x x x x x x x x 
4 f x x x x x x x 
5 x x x x x x x x 
6 x f x x x f x x 
7 x x x f x x x f
"""
# [(0, 0), (1, 2), (0, 4), (1, 6), (3, 7), (5, 6), (7, 7)]
# [(7, 7), (6, 5), (5, 7), (4, 5), (3, 3), (2, 1), (0, 0)]

moves = [
    (-1, 2),  # nahoru vlevo
    (1, 2),  # nahoru vpravo
    (-1, -2),  # dolu vlevo
    (1, -2),  # dolu vpravo
    (-2, 1),  # vlevo nahoru
    (-2, -1),  # vlevo dolu
    (2, 1),  # vpravo nahoru
    (2, -1),  # vpravo dolu
]
# moves = [
#     (1, 1),  # nahoru vlevo
#     (1, 1),  # nahoru vpravo
#     (-1, -1),  # dolu vlevo
#     (1, -1),  # dolu vpravo
#     (-1, 1),  # vlevo nahoru
#     (-1, -1),  # vlevo dolu
#     (1, 1),  # vpravo nahoru
#     (1, -1),  # vpravo dolu
# ]

# bude to aktualni pozice +- kam se chystam jit
# (0, 0) -> (2, 1) -> (4, 2)
# x = 0, y = 0 => x + dx, y + dx -> (0 + 2, 0 + 1) -> (2, 1)


def is_valid_move(x, y):
    # nejsem na x vlevo mene nez 0
    # nejsem na x vpravo vic nez delka hraciho planu
    # nejsem na y nahoru vice nez delka hraciho planu
    # nejsem na y dole mene nez 0
    # if 9 < 0 or 9 >=
    if x < 0 or x >= chessboard_size or y < 0 or y >= chessboard_size:
        return False

    return True


def shortest_path(start, end):
    distances = {start: 0}
    queue = [start]
    previous = {start: None}
    # print(distances, previous)

    while queue:
        x, y = queue.pop(0)

        if (x, y) == end:
            break

        for dx, dy in moves:  # (0, 0)
            new_x = x + dx  # 0 + 2
            new_y = y + dy  # 0 + 1

            if is_valid_move(new_x, new_y) and (new_x, new_y) not in distances:
                distances[(new_x, new_y)] = distances[(x, y)] + 1
                previous[(new_x, new_y)] = (x, y)
                queue.append((new_x, new_y))

    print(distances)
    print(previous)

    if end not in previous:
        return None

    path = [end]

    while previous[path[-1]] is not None:
        path.append(previous[path[-1]])

    path.reverse()
    return path


print(shortest_path((0, 0), (7, 7)))
# [(0, 0), (1, 2), (0, 4), (1, 6), (3, 7), (5, 6), (7, 7)]
# [(7, 7), (6, 5), (5, 7), (4, 5), (3, 3), (2, 1), (0, 0)]