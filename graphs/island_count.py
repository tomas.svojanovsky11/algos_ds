grid = [
    [0, 1, 0, 0, 0],
    [0, 1, 0, 0, 0],
    [0, 0, 0, 1, 0],
    [0, 0, 1, 1, 0],
    [1, 0, 0, 1, 1],
    [1, 1, 0, 0, 0],
]


def count_islands(grid):
    if not grid or not grid[0]:
        return 0

    num_rows = len(grid)
    num_cols = len(grid[0])
    visited = [[False for _ in range(num_cols)] for _ in range(num_rows)]
    count = 0

    def dfs(row, col):
        if row < 0 or col < 0 or row >= num_rows or col >= num_cols or grid[row][col] == 0 or visited[row][col]:
            return

        visited[row][col] = True
        dfs(row + 1, col)
        dfs(row - 1, col)
        dfs(row, col + 1)
        dfs(row, col - 1)

    for row in range(num_rows):
        for col in range(num_cols):
            if grid[row][col] == 1 and not visited[row][col]:
                dfs(row, col)
                count += 1

    return count


print(count_islands(grid))
