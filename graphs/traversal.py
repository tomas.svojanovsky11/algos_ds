"""
A ---→ C
|      |
|      |
↓      ↓
B      E
|
|
↓
D ---→ F
"""

graph = {
    "a": ['b', 'c'],
    "b": ['d'],
    "c": ['e'],
    "d": ['f'],
    "e": [],
    "f": [],
}


# stack - muzeme pouzit stack v programovacim jazyce - rekurzivne
# iterative - musime pouzit stack (list)
def deep_first_traversal(graph, source, dest):
    stack = [source]  # [b, c]

    while len(stack) > 0:
        current = stack.pop()  # c
        if current == dest:
            return True

        for neighbor in graph[current]:  # [e]
            stack.append(neighbor)

    return False



# queue
def breadth_first_traversal(graph, source):
    queue = [source]

    while len(queue) > 0:
        current = queue.pop(0)
        print(current)

        for neighbor in graph[current]:  # [e]
            queue.append(neighbor)


print(deep_first_traversal(graph, "d", "e"))
print("Next")
print(breadth_first_traversal(graph, "a"))

