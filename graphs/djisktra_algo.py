list1 = [None] * 7

#      (1) --- (4) ---- (5)
#    /  |       |       /|
# (0)   | ------|------- |
#    \  |/      |        |

#      (2) --- (3) ---- (6)
list1[0] = [
    {"to": 1, "weight": 3},
    {"to": 2, "weight": 1},
]
list1[1] = [
    {"to": 0, "weight": 3},
    {"to": 2, "weight": 4},
    {"to": 4, "weight": 1},
]
list1[2] = [
    {"to": 1, "weight": 4},
    {"to": 3, "weight": 7},
    {"to": 0, "weight": 1},
];
list1[3] = [
    {"to": 2, "weight": 7},
    {"to": 4, "weight": 5},
    {"to": 6, "weight": 1},
]
list1[4] = [
    {"to": 1, "weight": 1},
    {"to": 3, "weight": 5},
    {"to": 5, "weight": 2},
]
list1[5] = [
    {"to": 6, "weight": 1},
    {"to": 4, "weight": 2},
    {"to": 2, "weight": 18},
]
list1[6] = [
    {"to": 3, "weight": 1},
    {"to": 5, "weight": 1},
]


def has_unvisited(seen, dists):
    for i in range(len(seen)):
        if not seen[i] and dists[i] < float("inf"):
            return True

    return False


def get_lowest_unvisited(seen, dists):
    idx = -1
    lowest_distance = float("inf")

    for i in range(len(seen)):
        if seen[i]:
            continue

        if lowest_distance > dists[i]:
            lowest_distance = dists[i]
            idx = i

    return idx


def djisktra_solution(source, dest, array):
    array_length = len(array)

    seen = [False] * array_length
    dists = [float("inf")] * array_length
    prev = [-1] * array_length
    dists[source] = 0

    while has_unvisited(seen, dists):
        current = get_lowest_unvisited(seen, dists)
        seen[current] = True

        adjs = array[current]
        for i in range(len(adjs)):
            edge = adjs[i]
            if seen[edge["to"]]:
                continue

            dist = dists[current] + edge["weight"]
            if dist < dists[edge["to"]]:
                dists[edge["to"]] = dist
                prev[edge["to"]] = current

    out = []
    current = dest
    while prev[current] != -1:
        out.append(current)
        current = prev[current]

    out.append(source)
    out.reverse()
    return out


result = djisktra_solution(0, 6, list1)
print(result)
