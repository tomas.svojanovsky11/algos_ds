import itertools

distances = {
    'A': {'B': 5, 'C': 2, 'D': 3},
    'B': {'A': 5, 'C': 4, 'D': 1},
    'C': {'A': 2, 'B': 4, 'D': 6},
    'D': {'A': 3, 'B': 1, 'C': 6}
}
# 5 + 4 + 6 + 3 = 18


def calculate_distance(route):
    distance = 0
    for i in range(len(route)):
        city_1 = route[i]  # A
        city_2 = route[i + 1] if i < len(route) - 1 else route[0]  # B
        distance += distances[city_1][city_2]

    return distance


routes = list(itertools.permutations(distances.keys()))
# print(routes)
# 4!
shortest_distance = float("inf")
shortest_path = None
# print(calculate_distance(('A', 'B', 'C', 'D')))

start = "A"
filtered_routes = []

for route in routes:
    if route.index(start) == 0:
        filtered_routes.append(route)
print(filtered_routes)

for route in filtered_routes:
    distance = calculate_distance(route)
    # route, distance

    if distance < shortest_distance:
        shortest_distance = distance
        shortest_path = route

print(route)
print(shortest_distance)
