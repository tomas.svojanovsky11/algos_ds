def linear_search(needle, haystack):
    # Projdi cely list a hledej dokud nenajdes do hledat - OK
    # jinak vrat -1 - OK
    for i in range(len(haystack)):
        if haystack[i] == needle:
            return i

    return -1
