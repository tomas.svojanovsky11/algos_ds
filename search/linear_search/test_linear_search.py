from linear_search import linear_search


def test_linear_search():
    array = [5, 6, 77, 55, 22, 1, 3, 8]
    result = linear_search(3, array)
    assert result == 6

    result = linear_search(98, array)
    assert result == -1

# python3 -m pip install virtualenv
