def binary_search(needle, haystack):
    low = 0
    high = len(haystack) - 1

    while low <= high:
        mid = low + (high - low) // 2  # 0 + (8 - 0) / 2

        if haystack[mid] == needle:
            return mid
        elif haystack[mid] < needle:
            low = mid + 1  # kdyz je myslene cislo vetsi nez aktualni
        else:
            high = mid - 1  # kdyz je myslene cislo mensi nez aktualni

    return -1


