from search.binary_search.binary_search import binary_search


def test_search_binary_search():
    array = [1, 3, 6, 5, 8, 22, 55, 77]
    result = binary_search(3, array)
    assert result == 1

    result = binary_search(98, array)
    assert result == -1
