from typing import List


def capital_indexes(word: str) -> List[int]:
    result = []  # enumerate na retezci
    # projdi string znak po znaku a podivej se jestli je pismeno velke
    # pokud ano pridej do result, pokud ne, tak nic nedelej

    # enumerate, isupper, append
    for index, letter in enumerate(word):
        if letter.isupper():
            result.append(index)

    return result


result = capital_indexes("HeLlO")   # [0, 2, 4]
print(result)
result = capital_indexes("JezeceK")
print(result)
