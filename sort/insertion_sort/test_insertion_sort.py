from sort.insertion_sort.insertion_sort import insertion_sort


def test_insertion_sort():
    array = [-2, 45, 0, 11, -9, 88, -97, -202, 747]

    result = insertion_sort(array)
    assert result == [-202, -97, -9, -2, 0, 11, 45, 88, 747]
