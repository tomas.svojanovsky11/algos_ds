def selection_sort(array):
  size = len(array)

  for i in range(size):
    min_index = i # 2

    for j in range(i + 1, size):
      if array[j] < array[min_index]:  # 1 < 3
        min_index = j  # 2

    temp = array[i]
    array[i] = array[min_index]
    array[min_index] = temp

