from sort.selection_sort.selection_sort import selection_sort


def test_selection_sort():
    array = [-2, 45, 0, 11, -9, 88, -97, -202, 747]

    selection_sort(array)
    assert array == [-202, -97, -9, -2, 0, 11, 45, 88, 747]
