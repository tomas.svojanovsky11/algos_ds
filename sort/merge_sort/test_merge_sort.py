from sort.merge_sort.merge_sort import merge_sort


def test_merge_sort():
    array = [-2, 45, 0, 11, -9, 88, -97, -202, 747]

    merge_sort(array)
    assert array == [-202, -97, -9, -2, 0, 11, 45, 88, 747]
