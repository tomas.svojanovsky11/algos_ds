def merge_sort(arr):
    if len(arr) > 1:
        left_arr = arr[:len(arr) // 2]
        right_arr = arr[len(arr) // 2:]

        # print(left_arr, right_arr, "x")
        merge_sort(left_arr)
        merge_sort(right_arr)
        # print(left_arr, right_arr, "y")

        i = 0
        j = 0
        k = 0

        # left_arr = 0, right_arr = 11, arr = [0, 11]
        while i < len(left_arr) and j < len(right_arr):
            if left_arr[i] < right_arr[j]:
                arr[k] = left_arr[i]
                i += 1
            else:
                arr[k] = right_arr[j]
                j += 1
            k += 1

        while i < len(left_arr):
            arr[k] = left_arr[i]
            i += 1
            k += 1

        while j < len(right_arr):
            arr[k] = right_arr[j]
            j += 1
            k += 1


array = [-2, 45, 0, 11, -9, 88, -97, -202, 747]
merge_sort(array)
print(array)
