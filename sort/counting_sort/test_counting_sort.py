from sort.counting_sort.counting_sort import counting_sort


def test_counting_sort():
    array = [4, 2, 2, 8, 3, 3, 1]

    counting_sort(array)
    assert array == [1, 2, 2, 3, 3, 4, 8]