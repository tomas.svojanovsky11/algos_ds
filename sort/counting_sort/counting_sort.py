def counting_sort(array):
    size = len(array)
    output = [0] * size

    # max_value = max(array)

    count = [0] * 10

    for i in range(0, size):
        count[array[i]] += 1

    """
         staticka hodnota pro vycet prvku - abysme nemuseli hledat nejvetsi hodnotu
         kdyby se nachazelo v puvodnim listu cislo milion, tak rozsah bude milion prvku
    """
    for i in range(0, 10):
        count[i] += count[i - 1]

    # count = [1, 2, 4, 4, 6....]
    # count[array[5]] -> count[1] -> 4
    # [0, 0, 1, 1, 0, 3]

    i = size - 1
    while i >= 0:
        current = count[array[i]] - 1
        output[current] = array[i]
        count[array[i]] -= 1  # 4
        i -= 1

    for i in range(0, size):
        array[i] = output[i]


array = [1, 0, 3, 1, 3, 1]
counting_sort(array)
print(array)
