from bubble_sort import bubble_sort


def test_bubble_sort():
    unordered_list_1 = [4, 5, 8, 1, 6, 3, 7, 12, 9]
    bubble_sort(unordered_list_1)
    assert unordered_list_1 == [1, 3, 4, 5, 6, 7, 8, 9, 12]

    unordered_list_2 = [13, 101, 566, 88, 45, 11]
    bubble_sort(unordered_list_2)
    assert unordered_list_2 == [11, 13, 45, 88, 101, 566]
