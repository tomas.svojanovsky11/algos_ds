from typing import List


def bubble_sort(unordered_list: List[int]) -> None:
    size = len(unordered_list)

    # swapped = False

    for i in range(size):
        for j in range(0, size - i - 1):
            if unordered_list[j] > unordered_list[j + 1]:
                temp = unordered_list[j]
                unordered_list[j] = unordered_list[j + 1]
                unordered_list[j + 1] = temp

                # swapped = True

        # return
                # (unordered_list[j], unordered_list[j + 1]) = ([unordered_list[j + 1], [unordered_list[j]]])
                # 4, 5
                # 5, 4
                # temp = 4
                # 5, 4

# (n - 1) * (n - 2) .... 1 = n(n-1)/2