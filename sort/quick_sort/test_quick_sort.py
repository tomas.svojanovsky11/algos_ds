from sort.quick_sort.quick_sort import quick_sort


def test_quick_sort():
    array = [-2, 45, 0, 11, -9, 88, -97, -202, 747]

    quick_sort(array, 0, len(array) - 1)
    assert array == [-202, -97, -9, -2, 0, 11, 45, 88, 747]
