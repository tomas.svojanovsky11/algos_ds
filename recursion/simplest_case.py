def simplest_case(n: int) -> int:
    print(n, "x")

    if n == 1:
        return 1

    sum = n + simplest_case(n - 1)
    print(sum, "y")

    return sum

# 5 + jeste nevim foo (5 - 1)
# 4 + jeste nevim foo (4 - 1)
# 3 + jeste nevim foo (3 - 1)
# 2 + jeste nevim foo (2 - 1)
# 1 + jeste nevim -> narazil jsem na basecase -> 1


result = simplest_case(5)
print(result)
