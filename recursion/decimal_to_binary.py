def decimal_to_binary(value: int, result=""):
    if value == 0:
        return result

    result = str(value % 2) + result
    return decimal_to_binary(value // 2, result)


number = decimal_to_binary(233)
print(number)

# value=233, result="", new_result=1
# value=116, result=1, new_result=0 + 1
# value=58, result=01, new_result= 0 + 01

# 100 -> binary hodnotu
# 100 deleno 2 -> 0
# 50 deleno 2 -> 0
# 25 deleno 2 -> 1
# 12 deleno 2 ->
# 3 deleno 2 -> 1
# 1 deleno 2 -> 1
# 0

# 1100100 = 2

# 00