def is_palindrome(text: str) -> bool:
    # return text == text[::-1]
    # Base case
    length = len(text)
    if length == 0 or length == 1:  # Base Case
        return True

    # Recursive case
    # racecar
    # aceca
    # cec
    # e -> base case
    if text[0] == text[length - 1]:
        return is_palindrome(text[1:length - 1])

    return False


result = is_palindrome("racecar")
print(result)
