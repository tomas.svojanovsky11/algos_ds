def fib(n: int) -> int:
    # Base case je 0 nebo 1
    if n == 1 or n == 0:
        return n

    return fib(n - 1) + fib(n - 2)


# 0 1 1 2 3 5 8 13
print(fib(5))

# jeste nevim + jeste nevim fib(5 - 1) + fib(5 - 2)
