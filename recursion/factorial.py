def factorial(num: int):
    if num == 0:
        return 1

    return num * factorial(num - 1)


print(factorial(5))

# 5 * jeste nevim factorial(5 - 1) 5 * 24
# 4 * jeste nevim factorial(4 - 1) 4 * 6
# 3 * jeste nevim factorial(3 - 1) 3 * 2
# 2 * jeste nevim factorial(2 - 1) 2 * 1
# 1 * jeste nevim factorial(1 - 1) -> 1 * 1
# 0 -> base case

# 5!
# 5 * 4 * 3 * 2 * 1 = 120
# 0! = 1
