def binary_search(array, low, high, x):
    if high >= low:
        mid = low + (high - low) // 2

        if array[mid] == x:
            return mid

        elif array[mid] > x:
            return binary_search(array, low, mid - 1, x)

        else:
            return binary_search(array, mid + 1, high, x)

    else:
        return -1


array = [1, 3, 6, 5, 8, 22, 55, 77]
result = binary_search(array, 0, len(array) - 1, 22)
print(result)
