from typing import List

# Domaci ukol
# Zkusit vypsat list a zanest do nej vyslednou cestu

# ''' Navrhuji tento postup:
# 1. vypočítat vzdálenost k E
# 2. v případě střetu s W odbočka vlevo kontrola zda už jsem zde byl a když ne nový výpočet vzdálenosti'''


class Point:  # Parsing
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f'{self.x} {self.y}'


direction = [
    # x , y
    [-1, 0],  # vlevo
    [1, 0],   # vpravo
    [0, -1],  # dolu
    [0, 1],   # nahoru
]

maze = [
    "xxxxxxxxxx x",  # 0
    "x        x x",  # 1
    "x        x x",  # 2
    "x xxxxxxxx x",  # 3
    "x          x",  # 4
    "x xxxxxxxxxx",  # 5
]  # 01234567891011


def walk(maze: List[str], wall: str, curr: Point, end: Point, seen: List[List[bool]], path: List[Point]):
    # Jdu do zdi
    if maze[curr.y][curr.x] == wall:  # x = 1 y = 1
        return False
    # Mimo mapu
    if curr.x < 0 or curr.x >= len(maze[0]) or curr.y < 0 or curr.y > len(maze):
        return False
    # Jsem v cili
    if curr.x == end.x and curr.y == end.y:
        path.append(end)
        return True
    # Uz jsem navstivil pole
    if seen[curr.y][curr.x]:
        return False

    seen[curr.y][curr.x] = True
    path.append(curr)

    # projdeme vsechny mozne pohyby a ten co ti vrati True, tak tam pujdeme
    # pokud vsechno pujde dobre, tak se budu porad zanorovat
    for i in range(len(direction)):
        x, y = direction[i]  # [-1, 0]

        if walk(maze, wall, Point(curr.x + x, curr.y + y), end, seen, path):
            return True

    path.pop()

    return False


def solve(maze: List[str], wall: str, start: Point, end: Point):
    seen = []
    path = []
    for i in range(len(maze)):
        seen.append([False] * len(maze[0]))

    walk(maze, wall, start, end, seen, path)

    return path


result = solve(maze, "x", Point(10, 0), Point(1, 5))
print(result)

print(maze)


def draw_path(maze, path):
    data = list(map(lambda row: list(row), maze))
    for i in range(len(path)):
        p = path[i]
        if data[p.y][p.x]:
            data[p.y][p.x] = "O"

    return list(map(lambda p: "".join(p), data))


maze_with_path = draw_path(maze, result)
for row in maze_with_path:
    print(row)
